import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common";

@Controller('produtos')
export class ProdutosController{

    @Get()
    obterTodos(): string {
        return 'Lista todos os produtos';
    }

    @Get(':id')
    obterUm(@Param() params): string {
        return `Retorna os dados do produto ${params.id}`;
    }

    @Post()
    criar(@Body() produto): string {
        console.log(produto)
        return `produto criado`
    }

    @Put()
    alterar(@Body() produto): string {
        console.log(produto)
        return `produto atualizado`
    }

    @Delete(":id")
    apagar(@Param() params): string {
        
        return `apaga o produto ${params.id}`
    }
}